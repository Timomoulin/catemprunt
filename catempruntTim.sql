-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 27 avr. 2021 à 08:42
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `catemprunt`
--

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `emprunter`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `emprunter` (IN `numadh` INT(4), IN `numchat` INT(4))  begin
	insert into emprunt values(null,numchat,numadh,curdate(),null);
	update chat
	set etat=1
	where num=numchat;/*quand c'est fini on mets ;*/
end$$

DROP PROCEDURE IF EXISTS `inscrire`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `inscrire` (`nom` VARCHAR(30), `pren` VARCHAR(30), `em` VARCHAR(30), `md` VARCHAR(40))  begin
         insert into adherent values(null,nom,pren,em,md,curdate());
end$$

--
-- Fonctions
--
DROP FUNCTION IF EXISTS `existemail`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `existemail` (`em` VARCHAR(30)) RETURNS INT(1) begin
		if exists(select * from adherent where email=em) then
		     return 1;
			 else return 0;
			 end if;
			 
end$$

DROP FUNCTION IF EXISTS `login`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `login` (`em` VARCHAR(30), `md` VARCHAR(40)) RETURNS INT(1) begin
if exists(select * from adherent where email = em and mdp = md) then
    return "1";
  else return "0";
end if;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `adherent`
--

DROP TABLE IF EXISTS `adherent`;
CREATE TABLE IF NOT EXISTS `adherent` (
  `num` int(4) NOT NULL AUTO_INCREMENT,
  `nomadh` varchar(20) NOT NULL,
  `prenadh` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `mdp` varchar(40) NOT NULL,
  `dateadh` date DEFAULT NULL,
  PRIMARY KEY (`num`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `adherent`
--

INSERT INTO `adherent` (`num`, `nomadh`, `prenadh`, `email`, `mdp`, `dateadh`) VALUES
(1, 'FLORON', 'Emma', 'eldor@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '2021-05-14'),
(2, 'LAURET', 'Elle', 'eldort@free.fr', '', '2020-05-08'),
(3, 'DORI', 'Hilana', 'hildor@yahoo.com', '', '2020-04-01'),
(4, 'FRINPiOT', 'Joey', 'jofr@free.fr', '', '2017-04-11'),
(5, 'TERNETTURE', 'John', 'alter@gmail.com', '', '2017-12-08'),
(6, 'LEWIS', 'Jordan', 'alter@free.fr', '', '2017-01-24'),
(7, 'LEWIS', 'Alan', 'alter@yahoo.com', '', '2017-03-17'),
(8, 'DIKULO', 'Thomas', 'thidy@free.fr', '', '2017-11-14'),
(9, 'EPADALEGO', 'Jenny', 'jennep@yahoo.com', '', '2018-02-01'),
(10, 'MANTERO', 'Erwan', 'rudma@gmail.com', '', '2017-10-17');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` varchar(3) NOT NULL,
  `nomcateg` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nomcateg`) VALUES
('c1', 'Chats hybrides'),
('c2', 'Chats européens'),
('c3', 'Chats de race'),
('c4', 'Chats de gouttière');

-- --------------------------------------------------------

--
-- Structure de la table `chat`
--

DROP TABLE IF EXISTS `chat`;
CREATE TABLE IF NOT EXISTS `chat` (
  `num` int(4) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `annee` int(4) NOT NULL,
  `etat` int(1) NOT NULL,
  `photo` varchar(30) NOT NULL,
  `commentaire` text NOT NULL,
  `idcateg` varchar(3) NOT NULL,
  PRIMARY KEY (`num`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `chat`
--

INSERT INTO `chat` (`num`, `nom`, `annee`, `etat`, `photo`, `commentaire`, `idcateg`) VALUES
(24, 'Bengal', 2015, 1, '122', 'Le chat Bengal est doté d’une grande sensibilité et est très attentif à l’humeur des gens qui l’entourent. Ces caractéristiques, faisant de lui un très bon animal de compagnie', 'c1'),
(25, 'Savannah', 2013, 1, '123', 'Le chat Savannah a un comportement et caractère unique. C’est un chat athlétique. Il aime sauter, jouer et adore se cacher pour vous surprendre. Il est très affectueux et facile à vivre', 'c1'),
(26, 'Sacré de Birmanie', 2016, 1, '124', ' le Sacré de Birmanie est le compagnon idéal pour tous les membres de la famille. Il est apprécié pour son calme, sa délicatesse, mais aussi pour son côté joueur qui s’exprime assez souvent et sa très grande tendresse', 'c1'),
(27, 'Safari', 2013, 1, '126', 'Compte tenu de ses origines, le Safari possède toutes les caractéristiques d’un chat sauvage. Il a une forte personnalité. Pourtant, il a en réalité un tempérament très agréable, facile à vivre. C’est aussi un chat très affectueux qui apprécie la présence humaine, et particulièrement les enfants. Intelligent, le Safari est aussi malicieux.', 'c1'),
(28, 'Pixie Bob', 2018, 0, '127', 'En réalité, c’est un chat docile et très gentil. Il apprécie le confort de sa maison et a plaisir à se prélasser devant la cheminée. Vraie boule d’énergie, il est aussi dynamique et très actif. On le considère parfois comme un chien-chat car il est très attaché à ses maîtres', 'c1'),
(29, 'European Long Hair', 2018, 1, '128', 'Docile et affectueux, il aime la compagnie de l’homme', 'c2'),
(30, 'European Short Hair', 2017, 0, '129', 'Il aime l’indépendance et la liberté même s’il n’abandonne jamais son maître et sa maison.', 'c2'),
(31, 'European Short Hair', 2015, 1, '130', 'Il entretient, en général, de très bonnes relations avec les hommes et se montre attaché à son humain et à son lieu de vie.', 'c2'),
(32, 'European Short Hair', 2013, 0, '131', 'Ce chat aime avoir une certaine indépendance lorsqu’il décide qu’il en a besoin et appréciera toujours avoir accès à l’extérieur pour pouvoir assouvir son instinct de chasseur.', 'c2'),
(33, 'European Short Hair', 2016, 0, '132', 'Il a de grandes qualités de chasseur. Il est vraiment intelligent, très actif et vif.', 'c2'),
(34, 'European Short Hair', 2019, 1, '133', ' Il s\'agit la race de chat à poil court qui se rapproche le plus des chats sauvages, car il n’a que très peu été modifié par la sélection humaine.', 'c2'),
(35, 'British Long Hair', 2016, 1, '134', 'Le British longhair a hérité du caractère calme et posé de son cousin. Très présent sans être envahissant, il est adapté à la vie moderne, et offre toujours de beaux moments de sérénité au maître.', 'c3'),
(36, 'Highland straight', 2017, 0, '135', 'Le chats ou le chatons Highland Straight est un chat plutôt réservé, mais qui demeure malgré tout très affectueux. Intelligent, c’est un félin calme et silencieux, très proche de son maître.', 'c3'),
(37, 'Chartreux', 2015, 1, '136', 'Le Chartreux est l’ami de toute la famille. Calme, sociable, il se sent bien avec tout le monde. Il s’entend très bien avec les enfants. ', 'c3'),
(38, 'Maine Coon', 2013, 0, '137', 'C\'est un chat doté, quant il a été très \"socialisé\" depuis sa prime enfance, d\'un tempérament très équilibré, très confiant et très adaptable. Il s\'agit d\'un chat d\'appartement qui met chaleur et animation dans la maison.', 'c3'),
(39, 'Birman', 2016, 0, '138', 'Le chat Birmanest un chat au caractère splendide, un peu timide et très sensible ; il n’est absolument pas collant, ennuyeux ou bruyant.', 'c3'),
(40, 'Mau egyptien', 2019, 1, '139', ' Le chat Mau égyptien est un chat à l’intelligence vive, réservé et presque timide.', 'c3'),
(41, 'Chat de gouttière', 2018, 1, '140', 'le chat domestique est un animal curieux et perspicace.', 'c4'),
(42, 'Chat de gouttière ', 2017, 0, '141', 'le chat de gouttière est souvent très affectueux lorsqu’il a été mis en confiance.', 'c4'),
(43, 'Chat de gouttière', 2015, 1, '142', 'Il est suggéré de jouer quotidiennement avec votre félin, puisqu’il s’agit d’un animal énergique et facétieux.', 'c4'),
(44, 'Chat de gouttière', 2013, 0, '143', 'En règle générale, c’est un félin robuste et rustique.', 'c4'),
(45, 'Chat de gouttière', 2016, 0, '144', 'Tout comme ses caractéristiques physiques, le chat de gouttière développe un comportement qui dépend d’un individu à un autre. Néanmoins, on lui reconnaît un caractère affectueux, gentil et doux.', 'c4'),
(46, 'Chat de gouttière', 2019, 1, '145', ' Le chat commun, en plus de son physique propre à lui, possède en générale un caractère de vagabond, rusé, joueur mais aussi câlin.', 'c4');

-- --------------------------------------------------------

--
-- Structure de la table `emprunt`
--

DROP TABLE IF EXISTS `emprunt`;
CREATE TABLE IF NOT EXISTS `emprunt` (
  `num` int(6) NOT NULL AUTO_INCREMENT,
  `numchat` int(4) NOT NULL,
  `numadh` int(4) NOT NULL,
  `datemp` date NOT NULL,
  `dateretour` date DEFAULT NULL,
  PRIMARY KEY (`num`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `emprunt`
--

INSERT INTO `emprunt` (`num`, `numchat`, `numadh`, `datemp`, `dateretour`) VALUES
(15, 4, 10, '2020-01-22', '2020-01-22'),
(16, 5, 7, '2020-01-22', '2020-01-22'),
(17, 6, 8, '2020-03-22', NULL),
(18, 7, 11, '2020-03-22', NULL),
(19, 8, 6, '2020-04-05', NULL),
(20, 9, 1, '2018-04-07', NULL),
(21, 10, 1, '2018-02-07', NULL),
(22, 11, 1, '2020-02-07', NULL),
(23, 12, 1, '2020-02-07', NULL),
(24, 13, 1, '2020-02-07', NULL),
(25, 14, 1, '2020-02-07', NULL),
(26, 1, 26, '2021-04-27', NULL),
(27, 1, 26, '2021-04-27', NULL),
(28, 26, 1, '2021-04-27', NULL);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vcateg`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `vcateg`;
CREATE TABLE IF NOT EXISTS `vcateg` (
`id` varchar(3)
,`nomcateg` varchar(30)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_historique`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `v_historique`;
CREATE TABLE IF NOT EXISTS `v_historique` (
`num` int(6)
,`datemp` date
,`dateretour` date
,`email` varchar(30)
,`nom` varchar(30)
,`annee` int(4)
,`nomcateg` varchar(30)
,`date_emprunt` varchar(10)
,`date_retour` varchar(10)
,`infoschat` varchar(77)
);

-- --------------------------------------------------------

--
-- Structure de la vue `vcateg`
--
DROP TABLE IF EXISTS `vcateg`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vcateg`  AS  select `categorie`.`id` AS `id`,`categorie`.`nomcateg` AS `nomcateg` from `categorie` ;

-- --------------------------------------------------------

--
-- Structure de la vue `v_historique`
--
DROP TABLE IF EXISTS `v_historique`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_historique`  AS  select `emprunt`.`num` AS `num`,`emprunt`.`datemp` AS `datemp`,`emprunt`.`dateretour` AS `dateretour`,`adherent`.`email` AS `email`,`chat`.`nom` AS `nom`,`chat`.`annee` AS `annee`,`categorie`.`nomcateg` AS `nomcateg`,date_format(`emprunt`.`datemp`,'%d/%m/%Y') AS `date_emprunt`,date_format(`emprunt`.`dateretour`,'%d/%m/%Y') AS `date_retour`,concat(`chat`.`nom`,' - ',`categorie`.`nomcateg`,' . ',`chat`.`annee`) AS `infoschat` from (((`categorie` join `chat` on(`categorie`.`id` = `chat`.`idcateg`)) join `emprunt` on(`emprunt`.`numchat` = `chat`.`num`)) join `adherent` on(`emprunt`.`numadh` = `adherent`.`num`)) order by `emprunt`.`datemp` desc ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
