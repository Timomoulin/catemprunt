-- create view v_historique as
-- select emprunt.num,datemp,dateretour,email,nom,annee,nomcateg,
-- date_format(datemp, "%d/%m/%Y") as date_emprunt, date_format(dateretour,"%d/%m/%Y")as date_retour,
-- concat(nom," - ",nomcateg," . ",annee)as infoschat
-- from categorie
-- inner join chat
-- on categorie.id=chat.idcateg
-- inner join emprunt
-- on nomcateg=chat
-- inner join adherent
-- on emprunt.numadh = adherent.num
-- order by  datemp desc

create view v_historique as
select emprunt.num,datemp,dateretour,email,nom,annee,nomcateg,
date_format(datemp, "%d/%m/%Y") as date_emprunt, date_format(dateretour,"%d/%m/%Y")as date_retour,
concat(nom," - ",nomcateg," . ",annee)as infoschat
from categorie
inner join chat
on categorie.id=chat.idcateg
inner join emprunt
on emprunt.numchat=chat.num
inner join adherent
on emprunt.numadh = adherent.num
order by  datemp desc
