$("#valide").click
(
     function()
	 {
	    var email1=$("#email1").val();
		var email2=$("#email2").val();
		var mdp1=$("#mdp1").val();
		var mdp2=$("#mdp2").val();
		var nom=$("#nom").val();
		var prenom=$("#prenom").val();

		if(estRempli("saisie","orangered"))
		    {
			   if(!concordance(email1,email2))//voir si les mails sont egales
			   {
			       // $("#divmessage").html("Non concordance");
					//on colore en orange les deux zones
					$("#email1").css("background","orangered");
					$("#email2").css("background","orangered");
			   }
			   else if(!concordance(mdp1,mdp2))
			   {
			       // $("#divmessage").html("Non concordance");
					//on colore en orange les deux zones
					$("#mdp1").css("background","orangered");
					$("#mdp2").css("background","orangered");
			   }
			  else if(!mdpvalide(mdp1))
			  {
				  $("#mdp1").css("background","orangered");
			  }
			   else if(existemail("modeles/existemail.php",email1)=="1")
			   {
			       $("#divmessage").html("Adresse existante");
			   }
			   else
			   {
			      var res= inscrire("modeles/inscription.php",email1,nom,prenom,mdp1);
				  //$("#fldcentral").html(res);
				  effacer();
				 //retrait de la classe text-danger
				 // $("#divmessage").removeClasse('text-danger');
				 // on ajoute la classe success
				 $("#divmessage").css('color','pink');
				 $("#divmessage").html("Inscription réussie");
			   }

			}
	 }
);

$(".saisie").click
(
     function()
	 {
	    $(".saisie").css("background","white");
		$("#divmessage").html("");
	 }
);


function estRempli(classe,couleur)
{
	resultat=true;
	 $(".saisie").each
	 (
	       function()
		   {
			   var valeur=$(this).val();
			   if(!valeur)
			   {
				  resultat=false;
				  $(this).css({'background':couleur});// la couleur c'est une variable donc pas de ("")
			   }
		   }
	 );
	 return resultat;
}

/* procédure qui rétablit l'arrière-plan d'origine sur les zones concernées*/

function saisie(classe,macouleur)
{
	$('.saisie').each
	(
	function()
		{
			$(this).css('background','white');

		}
	);
}

function concordance(chaine1,chaine2)
{
	return chaine1==chaine2; //si les chaines sont identiques retoun vrai sinon faux
}


function mdpvalide(mdp)
{
		return mdp.length>=6;
}


/*function retablirCouleur(classe,couleur)
{
	$("."+classe).each
	(
	   $(this).css("background",couleur);

	);
}
*/

function existemail(urlphp,email)
{
    $.ajax
	   (
	        {
			    url:urlphp,
				data:{email:email},
				async:false,
				complete:function(resultat)
				{
				      res= resultat.responseText;

				}
			}

	   );
	   return res;
}

function inscrire(urlphp,email1,nom,prenom,mdp1)
{
    $.ajax //j'envoie via ajax les informations que j'ai remplis dans le formulaire
	   (
	        {
			    url:urlphp,
				data:{email:email1,nom:nom,prenom:prenom,mdp:mdp1},
				async:false,
				complete:function(resultat)
				{
				      res= resultat.responseText;

				}
			}

	   );
	   return res;
}

function effacer()
{
	$(".saisie").each
	(
	function()
	{
		$(this).val("");
	}



	);
}
