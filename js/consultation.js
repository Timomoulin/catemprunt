//variables globales
//indexcateg="c1";
indexcateg="0"; //mudamos para este para que todos os livros possam aparecer quando clicamos em "Tous les livres"

// on récup la chaine xml des catégories
xmlcateg=getXmlBase("categorie","0","0")// metemos 0 aqui porque queremos todas as colunas (escolhemos todas as categorias)

afficheListe("listecateg",xmlcateg,"categorie","id","nomcateg");
//appel de la procédure affichelivre
afficheChats();


///// procédures évènementielles /////
$("#r1, #r2").click
(
	function()
	{
		// récuperer l'id du objet courant
		id=$(this).attr('id');
		if(id==="r1")
		{
			afficheChats();
		}
		else
		{
			afficheDiapos();
		}
	}



);

// sélection dans la liste des catégories
$("#listecateg").change
(
	function()
	{
		indexcateg=$("#listecateg").val();
		afficheChats();
	}



);



// changement dans la liste




///// procésdures et fonctions /////
function afficheChats() //l'afficheChats va appeler une procédure
{
	xmlchats=""; //esta variavel foi declarada para que os livros possam aparecer em "Tous les livres"
	// on récup la chaine xml des livres correspondant à la catégorie
	if(indexcateg!="0") //este if et else foi posto para que todos os livros possam ser apresentados quando clicamos em "tous les livres"
	{
		xmlchats=getXmlBase("chat", "idcateg",indexcateg); // indexcateg sans ("") parce que c'est une variable //
	//ao invés de meter a tabela "livre" metemos a de "vlivredit" porque tem todas as categorias que precisamos.
	//a tabela "livre" nao tem o nome do editor
	//alert(xmllivres);
	//getXmlBase c'est une fonction du modèle contrôleur
	}
	else
	{
		xmlchats=getXmlBase("chat", "idcateg",indexcateg);

	}
	// alert(xmlchats);
	chaine=""; //variable chaine qui va concatener
	// on parse la chaine xml // parse = séparer les chaines ex. nom téléphone...
	$(xmlchats).find("chat").each
	(
		function()
		{
			
			var num=$(this).find("num").text();//num des chats
			var nom=$(this).find("nom").text();
			var annee=$(this).find("annee").text();
			var commentaire=$(this).find("commentaire").text();
			var etat = $(this).find("etat").text();
			
			var photo=$(this).find("photo").text();
			// var nomedit=$(this).find("nomedit").text();
			//chaine+=titre+"<br/>"+annee+"<br/>"+commentaire+ // tiramos esta linha porque foi so para um teste (nao tem nada de design)
			//"<br/><br/>";

			chaine+=modeleChat(num,nom,annee,commentaire, etat,photo);//les variables
		}

	);

	$("#divchats").html(chaine);
}

function afficheDiapos()
{
	xmlchats=""; //esta variavel foi declarada para que os livros possam aparecer em "Tous les livres"
	// on récup la chaine xml des livres correspondant à la catégorie
	if(indexcateg!="0") //este if et else foi posto para que todos os livros possam ser apresentados quando clicamos em "tous les livres"
	{
	xmlchats=getXmlBase("chat", "idcateg",indexcateg); // indexcateg sans ("") parce que c'est une variable //
	//ao invés de meter a tabela "livre" metemos a de "vlivredit" porque tem todas as categorias que precisamos.
	//a tabela "livre" nao tem o nome do editor
	//alert(xmllivres);
	//getXmlBase c'est une fonction du modèle contrôleur
	}
	else
	{
		xmlchats=getXmlBase("chat", "0","0");
	}
	chaine="";
	$(xmlchats).find("chat").each
	(
		function()
		{
			var num=$(this).find("num").text();
			var nom=$(this).find("nom").text();//fizemos este ponto depois
			var indexphoto=$(this).find("photo").text();
			var commentaire=$(this).find("commentaire").text();
			chaine+=`<img id=${num} title=${commentaire} class='classephoto pl-2 pt-2' src="photos/${indexphoto}.jpg" /><div id="photocommentaire"></div>`;
alert(chaine);
		}
	);
		$("#divlivres").html(chaine);
}

function modeleChat(num,nom,annee,commentaire, etat,photo)
{
	// metemos uma outra variavel "chaine" mesmo tendo utilizado uma com mesmo nome na "procedure" anterior
	//como nao é uma vaiavel global nao tem problema
	chaine="";
	//chaine+="<div class='bg-light classelivre'>"; //meti em comentario pk o meu CSS nao funciona com a classelivre
	chaine+="<div class= 'text-center bg-light border border-dark'>";
	chaine+="<h2 class='text-warning text-center'>"+nom+"</h3>";
	chaine+="<h4 class='text-secondary text-center'>";
	chaine+=" "+annee+"</h4>";
	chainephoto="photos/"+photo+".jpg";
	img="<img src='"+chainephoto+"' width='25%' height='25%' class='img-fluid' />";
	chaine+=img;
	chaine+="<h6 class='text-justify p-2'>"+commentaire+"</h6>";

	//teste livre dispo ou pas
	if(etat=="0" && sessionStorage.id )
	{
		chaine+="<h4 class='text-success text-left pl-1'><strong>Disponible</strong></h4>";
		//afficher le button pour réserver
		chaine+="<button id='"+num+"' type='button' role='button' class='classereserve btn btn-success btn-right btn-sm mb-2'>Réserver</button>";
	}
	else
	{
		chaine+="<h4 class='text-danger text-left pl-1'><strong>Indisponible</strong></h4>";
	}


	chaine+="</div>";
	chaine+="<br /><br/>";
	return chaine;
}

//click sur un bouton de reservation d'un livre
$(".classereserve").live
(
	"click",function()
	{
		var idChat = $(this).attr('id');
		email=sessionStorage.id;
		xmladh=getXmlBase("adherent","email",email);
		num=getElement(xmladh,"adherent","email",email,"num");
		res=emprunter("modeles/emprunter.php",num,idChat);
		console.log(res);
		afficheChats();


	}


);
//fonction pour ajouter un nouvel emprunt
function emprunter(urlphp,num,cote)
{
    $.ajax //j'envoie via ajax les informations que j'ai remplis dans le formulaire
	   (
	        {
			    url:urlphp,
				data:{num:num,numChat:cote},
				async:false,
				complete:function(resultat)
				{
				      res= resultat.responseText;

				}
			}

	   );
	   console.log(cote);
	   return res;
}

// fonctions utilitaires

 function getXmlBase(nomtable,clef,valeur)
{
 $.ajax
	  (
			{
			     url:"modeles/sqlxml.php",
				 datatype:"xml",//datatype c'est le type de données
				 data:{nomtable:nomtable,clef:clef,valeur:valeur},
				 async:false,//
				 success:function(resultat)
				 {
				 xml=resultat;
				 }
			}
	  );

  return xml;
}
