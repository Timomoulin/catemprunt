//click sur le bouton

$("#valider").click
(
	function()
	{
		var resultat=estvide("saisie");
		if(!resultat)
		{
			email=$("#email").val();
			var mdp=$("#mdp").val();

			var connex=connexion("modeles/connexion.php",email,mdp);
			alert(connex);
			if(connex=="0")
			{
				$("#message").html("Informations incorrectes");
				$("#message").css("color","pink");
			}
			else
			{
				$("#message").html("Connexion réussie");
				$("#message").css("color","forestgreen");
				sessionStorage.id=email;
				var xmladh=getXmlBase("adherent","email",email);
				var prenadh=getElement(xmladh,"adherent","email",email,"prenadh");
				$("#accueil").html("Bonjour "+prenadh);
				$("#accueil").css("color","red");
			}
		}


	}
);

// click sur un des élèments du formulaire
//classe"saisie"

$(".saisie").click
(
	function()
	{
		//annulation des surbrillance précédentes
		$(".saisie").css("background","white");

		//mise en surbrillance de la zone concernée
		$(this).css("background","lightyellow");
	}
);

////// fonctions utilisateur
function connexion(urlphp,email,mdp)
{
    $.ajax
	   (
	        {
			    url:urlphp,
				data:{email:email,mdp:mdp},
				async:false,
				complete:function(resultat)
				{
				      res= resultat.responseText;

				}
			}

	   );
	   return res;
}

function estvide(maclasse)
{
	classe="."+maclasse;
	vide=false;
	$(classe).each
	(
	      function()
		  {
			   valeur=$(this).val();

			   if(!valeur)
			   {
				   vide=true;
				   $(this).css("background","red");
			   }
		  }
	);

	return vide;
}
